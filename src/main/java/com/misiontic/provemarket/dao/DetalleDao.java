/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.provemarket.dao;

import com.misiontic.provemarket.model.Detalle;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Nodier
 */
public interface DetalleDao extends CrudRepository<Detalle, Integer>{
    
}
