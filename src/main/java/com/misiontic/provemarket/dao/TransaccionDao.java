/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.provemarket.dao;

import com.misiontic.provemarket.model.Transaccion;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Nodier
 */
public interface TransaccionDao extends CrudRepository<Transaccion, Integer>{
    
}
