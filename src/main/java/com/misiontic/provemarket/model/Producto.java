/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.provemarket.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nodier
 */

@Entity
@Table(name="producto")
public class Producto implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idProducto")
    private Integer idProducto;
    
    @ManyToOne
    @JoinColumn(name="idTienda")
    private Tienda tienda;
    
    @Column(name="referencia")
    private String referencia;
    
    @Column(name="nombre")
    private String nombre;
    
    @Column(name="cantidad")
    private double cantidad;
    
    @Column(name="cantidadMinima")
    private double cantidadMinima;
    
    @Column(name="precioCompra")
    private double precioCompra;
    
    @Column(name="precioVenta")
    private double precioVenta;

    public Producto() {
    }

    public Producto(Integer idProducto, Tienda tienda, String referencia, String nombre, double cantidad, double cantidadMinima, double precioCompra, double precioVenta) {
        this.idProducto = idProducto;
        this.tienda = tienda;
        this.referencia = referencia;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.cantidadMinima = cantidadMinima;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCantidadMinima() {
        return cantidadMinima;
    }

    public void setCantidadMinima(double cantidadMinima) {
        this.cantidadMinima = cantidadMinima;
    }

    public double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }
    
    

}
