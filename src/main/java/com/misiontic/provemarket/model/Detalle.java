/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.provemarket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nodier
 */

@Entity
@Table(name="detalle")
public class Detalle {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idDetalle")
    private Integer idDetalle;
    
    @ManyToOne
    @JoinColumn(name="idTransaccion")
    private Transaccion transaccion;
    
    @ManyToOne
    @JoinColumn(name="idProducto")
    private Producto producto;
    
    @Column(name="cantidad")
    private double cantidad;
    
    @Column(name="total")
    private double total;

    public Detalle() {
    }
    
    public Detalle(Integer idDetalle, Transaccion transaccion, Producto producto, double cantidad, double total) {
        this.idDetalle = idDetalle;
        this.transaccion = transaccion;
        this.producto = producto;
        this.cantidad = cantidad;
        this.total = total;
    }

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
       
}
