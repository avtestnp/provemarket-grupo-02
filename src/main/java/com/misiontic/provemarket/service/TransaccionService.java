/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.provemarket.service;

import com.misiontic.provemarket.model.Transaccion;
import java.util.List;

/**
 *
 * @author Nodier
 */
public interface TransaccionService {
    public Transaccion save(Transaccion transaccion);
    public void delete(Integer id);
    public Transaccion findById(Integer id);
    public List<Transaccion> findAll();
}
