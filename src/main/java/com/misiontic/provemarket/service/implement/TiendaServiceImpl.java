/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.provemarket.service.implement;

import com.misiontic.provemarket.dao.TiendaDao;
import com.misiontic.provemarket.model.Tienda;
import com.misiontic.provemarket.service.TiendaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nodier
 */
@Service
public class TiendaServiceImpl implements TiendaService{
    
    @Autowired
    private TiendaDao tiendaDao;

    @Override
    @Transactional(readOnly = false)
    public Tienda save(Tienda tienda) {
        return tiendaDao.save(tienda);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        tiendaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Tienda findById(Integer id) {
        return tiendaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tienda> findAll() {
        return (List<Tienda>) tiendaDao.findAll();
    }
    
}
