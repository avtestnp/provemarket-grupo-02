/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.provemarket.service;

import com.misiontic.provemarket.model.Tienda;
import java.util.List;

/**
 *
 * @author Nodier
 */
public interface TiendaService {
    public Tienda save(Tienda tienda);
    public void delete(Integer id);
    public Tienda findById(Integer id);
    public List<Tienda> findAll();
}
