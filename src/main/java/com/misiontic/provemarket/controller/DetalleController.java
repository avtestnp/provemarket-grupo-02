/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.provemarket.controller;

import com.misiontic.provemarket.model.Detalle;
import com.misiontic.provemarket.service.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nodier
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class DetalleController {
    @Autowired
    private DetalleService detalleService;

    @PostMapping(value = "/")
    public ResponseEntity<Detalle> agregar(@RequestBody Detalle detalle) {
        Detalle obj = detalleService.save(detalle);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Detalle> eliminar(@PathVariable Integer id) {
        Detalle obj = detalleService.findById(id);
        if (obj != null) {
            detalleService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);

    }

    @PutMapping(value = "/")
    public ResponseEntity<Detalle> editar(@RequestBody Detalle detalle) {
        Detalle obj = detalleService.findById(detalle.getIdDetalle());
        if (obj != null) {
            obj.setCantidad(detalle.getCantidad());            
            obj.setTotal(detalle.getTotal());

            detalleService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Detalle> consultarTodo() {
        return detalleService.findAll();
    }

    @GetMapping("/list/{id}")
    public Detalle consultaPorId(@PathVariable Integer id) {
        return detalleService.findById(id);
    }
}
