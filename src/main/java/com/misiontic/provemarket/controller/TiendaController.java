/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.provemarket.controller;

import com.misiontic.provemarket.model.Tienda;
import com.misiontic.provemarket.service.TiendaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nodier
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/tienda")
public class TiendaController {

    @Autowired
    private TiendaService tiendaService;

    @PostMapping(value = "/")
    public ResponseEntity<Tienda> agregar(@RequestBody Tienda tienda) {
        Tienda obj = tiendaService.save(tienda);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Tienda> eliminar(@PathVariable Integer id) {
        Tienda obj = tiendaService.findById(id);
        if (obj != null) {
            tiendaService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);

    }

    @PutMapping(value = "/")
    public ResponseEntity<Tienda> editar(@RequestBody Tienda tienda) {
        Tienda obj = tiendaService.findById(tienda.getIdTienda());
        if (obj != null) {
            obj.setNombre(tienda.getNombre());
            obj.setNit(tienda.getNit());
            obj.setDireccion(tienda.getDireccion());
            obj.setTelefono(tienda.getTelefono());
            tiendaService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Tienda> consultarTodo() {
        return tiendaService.findAll();
    }

    @GetMapping("/list/{id}")
    public Tienda consultaPorId(@PathVariable Integer id) {
        return tiendaService.findById(id);
    }

}
