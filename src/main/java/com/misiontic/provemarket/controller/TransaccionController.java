/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.provemarket.controller;

import com.misiontic.provemarket.model.Transaccion;
import com.misiontic.provemarket.service.TransaccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nodier
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/transaccion")
public class TransaccionController {
    @Autowired
    private TransaccionService transaccionService;

    @PostMapping(value = "/")
    public ResponseEntity<Transaccion> agregar(@RequestBody Transaccion transaccion) {
        Transaccion obj = transaccionService.save(transaccion);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Transaccion> eliminar(@PathVariable Integer id) {
        Transaccion obj = transaccionService.findById(id);
        if (obj != null) {
            transaccionService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);

    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Transaccion> editar(@RequestBody Transaccion transaccion) {
        Transaccion obj = transaccionService.findById(transaccion.getIdTransaccion());
        if (obj != null) {
            obj.setTipo(transaccion.getTipo());
            obj.setCodigo(transaccion.getCodigo());
            obj.setFecha(transaccion.getFecha());
            obj.setValorTotal(transaccion.getValorTotal());
            
            transaccionService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Transaccion> consultarTodo() {
        return transaccionService.findAll();
    }

    @GetMapping("/list/{id}")
    public Transaccion consultaPorId(@PathVariable Integer id) {
        return transaccionService.findById(id);
    }

}
