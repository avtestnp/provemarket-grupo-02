package com.misiontic.provemarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvemarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvemarketApplication.class, args);
	}

}
