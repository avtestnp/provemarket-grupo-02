-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-09-2022 a las 17:11:10
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `provemarket`
--

CREATE DATABASE IF NOT EXISTS `provemarket`;
USE `provemarket`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `idDetalle` int(12) NOT NULL,
  `idTransaccion` int(8) NOT NULL,
  `idProducto` int(8) NOT NULL,
  `cantidad` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`idDetalle`, `idTransaccion`, `idProducto`, `cantidad`, `total`) VALUES
(3, 1, 31, 8, 16000),
(4, 1, 32, 5, 7500),
(5, 1, 33, 8, 40000),
(6, 2, 27, 180, 54000),
(7, 2, 28, 15, 30000),
(8, 2, 29, 20, 280000),
(9, 2, 30, 20, 100000),
(10, 3, 16, 15, 30000),
(11, 3, 17, 50, 44000),
(12, 4, 1, 1, 2500),
(13, 4, 10, 1, 1500),
(14, 4, 11, 2, 3600),
(15, 5, 12, 2, 7000),
(16, 5, 13, 2, 10000),
(17, 5, 18, 2, 16000),
(18, 5, 33, 1, 7000),
(19, 6, 19, 2, 17000),
(20, 6, 23, 2, 28000),
(21, 7, 31, 2, 6000),
(22, 7, 35, 1, 12000),
(23, 8, 19, 2, 16000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(8) NOT NULL,
  `idTienda` int(4) NOT NULL,
  `referencia` varchar(15) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` double NOT NULL,
  `cantidadMinima` double NOT NULL,
  `precioCompra` double NOT NULL,
  `precioVenta` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `idTienda`, `referencia`, `nombre`, `cantidad`, `cantidadMinima`, `precioCompra`, `precioVenta`) VALUES
(1, 1, '1111', 'Azucar', 20, 5, 2000, 2500),
(10, 1, '1112', 'Sal', 25, 5, 1000, 1500),
(11, 1, '1113', 'Arroz', 30, 10, 1200, 1800),
(12, 1, '1114', 'Lenteja', 20, 5, 2500, 3500),
(13, 1, '1115', 'Frijol', 30, 10, 4000, 5000),
(14, 1, '1116', 'Arveja', 20, 5, 2000, 3000),
(15, 1, '1117', 'Garvanzo', 25, 10, 3000, 4000),
(16, 1, '2111', 'Leche entera', 20, 5, 2000, 3000),
(17, 1, '2112', 'Leche deslactosada', 30, 10, 2200, 3200),
(18, 1, '2113', 'Queso campesino', 20, 5, 6000, 8000),
(19, 1, '2114', 'Queso mozzarela', 20, 5, 7000, 8500),
(20, 1, '2115', 'Yogur', 15, 10, 1600, 2400),
(21, 1, '2116', 'Kumis', 20, 5, 1500, 2000),
(22, 1, '3111', 'Jamón tradicional', 20, 5, 8000, 12000),
(23, 1, '3112', 'Jamón Pietrán', 15, 5, 9000, 14000),
(24, 1, '3113', 'Salchicha parrillera', 20, 5, 8000, 12000),
(25, 1, '3114', 'Salchicha tradicional', 15, 5, 6500, 10000),
(26, 1, '3115', 'Mortdela', 20, 10, 7000, 9000),
(27, 1, '4111', 'Huevos', 180, 30, 300, 500),
(28, 1, '4112', 'Pan bolsa', 20, 5, 2000, 3000),
(29, 1, '4113', 'Café', 20, 5, 14000, 18000),
(30, 1, '4114', 'Chocolate', 20, 5, 5000, 7000),
(31, 1, '5111', 'Jabón tocador', 10, 5, 2000, 3000),
(32, 1, '5112', 'Jabón Rey', 8, 5, 1500, 2500),
(33, 1, '5113', 'Crema dental', 10, 3, 5000, 7000),
(34, 1, '5114', 'Cepillo dental', 10, 3, 3000, 5000),
(35, 1, '5115', 'Shampoo', 10, 3, 8000, 12000),
(36, 1, '5116', 'Desodorante', 15, 5, 7000, 10000),
(37, 1, '5117', 'Rinse', 8, 3, 9000, 12500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `idTienda` int(4) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `nit` varchar(12) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`idTienda`, `nombre`, `nit`, `direccion`, `telefono`) VALUES
(1, 'ProveMarket1', '800123456', 'Carrera 1 # 1-11', '6011111111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `idTransaccion` int(8) NOT NULL,
  `idUsuario` int(4) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  `codigo` varchar(12) NOT NULL,
  `fecha` date NOT NULL,
  `valorTotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`idTransaccion`, `idUsuario`, `tipo`, `codigo`, `fecha`, `valorTotal`) VALUES
(1, 1, '01', '800111222', '2022-09-02', 63500),
(2, 1, '01', '800111333', '2022-09-03', 464000),
(3, 1, '01', '800111444', '2022-09-03', 74000),
(4, 1, '02', '37780780', '2022-09-02', 7600),
(5, 1, '02', '52300300', '2022-09-04', 40000),
(6, 1, '02', '45320320', '2022-09-05', 45000),
(7, 1, '02', '67430430', '2022-09-05', 18000),
(8, 1, '02', '78210210', '2022-09-05', 16000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(4) NOT NULL,
  `idTienda` int(4) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `tipoDoc` varchar(2) NOT NULL,
  `numeroDoc` varchar(12) NOT NULL,
  `dierccion` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `genero` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `idTienda`, `nombre`, `password`, `tipoDoc`, `numeroDoc`, `dierccion`, `telefono`, `email`, `genero`) VALUES
(1, 1, 'Pedro Pérez', '123456', 'CC', '37123456', 'Carrera 1 # 1-15', '6012222222', 'pedroperez@hotmail.com', 'Masculino');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`idDetalle`),
  ADD KEY `fk_detalle_producto` (`idProducto`),
  ADD KEY `fk_detalle_transaccion` (`idTransaccion`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`),
  ADD KEY `fk_producto_tienda` (`idTienda`) USING BTREE;

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`idTienda`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`idTransaccion`),
  ADD KEY `fk_transaccion_usuario` (`idUsuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `fk_usuario_tienda` (`idTienda`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle`
--
ALTER TABLE `detalle`
  MODIFY `idDetalle` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `idTienda` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `idTransaccion` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detalle_transaccion` FOREIGN KEY (`idTransaccion`) REFERENCES `transaccion` (`idTransaccion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_tienda` FOREIGN KEY (`idTienda`) REFERENCES `tienda` (`idTienda`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `fk_transaccion_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idTienda`) REFERENCES `tienda` (`idTienda`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
